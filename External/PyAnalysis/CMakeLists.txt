# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building/installing python "analysis" modules.
#

# The name of the package:
atlas_subdir( PyAnalysis )

# In release rebuild mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Figure out where to take Python from:
if( ATLAS_BUILD_PYTHON )
   set( PYTHON_EXECUTABLE ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python )
else()
   find_package( PythonInterp 2.7 REQUIRED )
endif()

# Install the find module(s):
install( FILES cmake/FindNumPy.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Figure out whether we need to do anything. If we are not building Python as
# part of the build, and NumPy and pip are both already available, then let's
# not do anything else.
if( NOT ATLAS_BUILD_PYTHON )
   list( APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake )
   find_package( NumPy QUIET )
   find_package( pip QUIET )
   if( NUMPY_FOUND AND PIP_FOUND )
      return()
   endif()
endif()

# Setup the build/runtime environment for the python analysis packages:
configure_file(
   ${CMAKE_CURRENT_SOURCE_DIR}/cmake/PyAnalysisEnvironmentConfig.cmake.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyAnalysisEnvironmentConfig.cmake
   @ONLY )
set( PyAnalysisEnvironment_DIR
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
   CACHE PATH "Location of PyAnalysisEnvironmentConfig.cmake" )
find_package( PyAnalysisEnvironment REQUIRED )

# Tell the user what's happening:
message( STATUS "Building python (analysis) modules as part of this project" )

# A common installation directory for all python externals of the package:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyAnalysisBuild )
set( _sitePkgsDir ${_buildDir}/lib/python2.7/site-packages )

# Build/install setuptools:
set( _source
   "http://cern.ch/lcgpackages/tarFiles/sources/setuptools-20.1.1.tar.gz" )
set( _md5 "10a0f4feb9f2ea99acf634c8d7136d6d" )
ExternalProject_Add( setuptools
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
   BUILD_COMMAND ${CMAKE_BINARY_DIR}/atlas_build_run.sh
   ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py build
   INSTALL_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
   ${CMAKE_BINARY_DIR}/atlas_build_run.sh
   ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
   COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeScripts.sh
   "${_buildDir}/bin/easy_install*"
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
add_dependencies( Package_PyAnalysis setuptools )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( setuptools Python )
endif()

if( NOT NUMPY_FOUND OR ATLAS_BUILD_PYTHON )
   # Build/install numpy:
   set( _source
      "http://cern.ch/lcgpackages/tarFiles/sources/numpy-1.13.3.tar.gz" )
   set( _md5 "88fd33c051edc975361d5de7278128b0" )
   ExternalProject_Add( numpy
      PREFIX ${CMAKE_BINARY_DIR}
      INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
      URL ${_source}
      URL_MD5 ${_md5}
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
      BUILD_COMMAND ${CMAKE_COMMAND} -E env --unset=SHELL
      ${CMAKE_BINARY_DIR}/atlas_build_run.sh
      ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py build
      INSTALL_COMMAND ${CMAKE_COMMAND} -E env --unset=SHELL
      PYTHONPATH=${_sitePkgsDir}
      ${CMAKE_BINARY_DIR}/atlas_build_run.sh
      ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
      COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeScripts.sh
      "${_buildDir}/bin/f2py*"
      COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
   add_dependencies( Package_PyAnalysis numpy )
   add_dependencies( numpy setuptools )
endif()

if( NOT PIP_FOUND OR ATLAS_BUILD_PYTHON )
   # Build/install wheel:
   set( _source
      "http://cern.ch/lcgpackages/tarFiles/sources/wheel-0.29.0.tar.gz" )
   set( _md5 "555a67e4507cedee23a0deb9651e452f" )
   ExternalProject_Add( wheel
      PREFIX ${CMAKE_BINARY_DIR}
      INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
      URL ${_source}
      URL_MD5 ${_md5}
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
      BUILD_COMMAND ${CMAKE_BINARY_DIR}/atlas_build_run.sh
      ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py build
      INSTALL_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
      ${CMAKE_BINARY_DIR}/atlas_build_run.sh
      ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
      COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeScripts.sh
      "${_buildDir}/bin/wheel*"
      COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
   add_dependencies( Package_PyAnalysis wheel )
   add_dependencies( wheel setuptools )
   if( ATLAS_BUILD_PYTHON )
      add_dependencies( wheel Python )
   endif()

   # Build/install pip:
   set( _source
      "http://cern.ch/lcgpackages/tarFiles/sources/pip-9.0.1.tar.gz" )
   set( _md5 "35f01da33009719497f01a4ba69d63c9" )
   ExternalProject_Add( pip
      PREFIX ${CMAKE_BINARY_DIR}
      INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
      URL ${_source}
      URL_MD5 ${_md5}
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
      BUILD_COMMAND ${CMAKE_BINARY_DIR}/atlas_build_run.sh
      ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py build
      INSTALL_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
      ${CMAKE_BINARY_DIR}/atlas_build_run.sh
      ${PYTHON_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
      COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeScripts.sh
      "${_buildDir}/bin/pip*"
      COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
   add_dependencies( Package_PyAnalysis pip )
   add_dependencies( pip setuptools wheel )
   if( ATLAS_BUILD_PYTHON )
      add_dependencies( pip Python )
   endif()
endif()

# Install all built modules at the same time:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Test that, whatever the setup, we are able to use pip to install (as an
# example) the cookiecutter project.
set( _pipInstallDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/cookiecutterTest )
atlas_add_test( pip_install
   SCRIPT pip install --target=${_pipInstallDir} cookiecutter )
